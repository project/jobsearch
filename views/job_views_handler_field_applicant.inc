<?php

/**
* Field handler for variable values
*/
class job_views_handler_field_applicant extends views_handler_field {
  function render($values) {
     $obj = new stdClass();
     $obj = user_load(array('uid' => $values->variable_value));
     return theme('username', $obj);   
  }
}